import mongoose from "mongoose";
import PostMessage from "../models/postMessage.js";

export const getPosts = async (req, res) => {
  //   res.send("THIS WORKS!");
  try {
    const postMessages = await PostMessage.find();
    // console.log(postMessages);
    res.status(200).json(postMessages);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

export const createPost = async (req, res) => {
  //   res.send("Post Creation");

  const post = req.body;
  const newPost = new PostMessage(post);
  try {
    await newPost.save();
    resizeBy.status(201).json(newPost);
  } catch (error) {
    res.status(409).json({ message: error.message });
  }
};

export const updatePost = async (req, res) => {
  const { id: _id } = req.params;
  const post = req.body;

  await PostMessage.updateOne({ _id: _id }, { $set: post }).then((data) =>
    res.send(data)
  );

  // if (!mongoose.Types.ObjectId.isValid())
  //   return res.status(404).send("No Post with that id");

  // const updatedPost = PostMessage.findByIdAndUpdate(_id, post, { new: true });
  // return res.json(updatedPost);
};

export const deletePost = async (req, res) => {
  console.log(req.params);
  const { id } = req.params;

  await PostMessage.deleteOne({ id: id });

  // if (!mongoose.Types.ObjectId.isValid(_id))
  //   return res.status(404).send("No Post with that id");

  // await PostMessage.findByIdAndRemove(_id);

  res.json({ message: "Post deleted successfully" });
};
